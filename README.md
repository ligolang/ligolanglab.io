# === NOT USED - please go to https://gitlab.com/ligolang/ligo instead. ===


# Ligo documentation

## Running the documentation website

The website will be available at [http://localhost:3000](http://localhost:3000)

```zsh
docker-compose up
```